import sucrase from '@rollup/plugin-sucrase'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import postcss from 'rollup-plugin-postcss'

export default {
  input: 'client/main.tsx',
  output: {
    file: 'public/bundle.js',
    format: 'cjs'
  },
  plugins: [
    resolve({
      extensions: ['.js', '.ts', '.tsx']
    }),
    commonjs(),
    sucrase({
      exclude: ['node_modules/**'],
      transforms: ['jsx', 'typescript'],
      jsxPragma: 'm',
      jsxFragmentPragma: '\'[\''
    }),
    postcss({
      plugins: [],
      extract: true
    })
  ]
}
