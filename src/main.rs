use askama::Template;
use tide::{Request, Response};

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {}

async fn index_handler(_req: Request<()>) -> Result<Response, tide::Error> {
    let res: Response = IndexTemplate {}.into();
    Ok(res)
}

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    let mut app = tide::new();
    app.at("/").get(index_handler);
    app.at("/static").serve_dir("public/")?;
    println!("Starting at http://localhost:8080");
    app.listen("127.0.0.1:8080").await?;
    Ok(())
}
